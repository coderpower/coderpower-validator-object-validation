### Object Validation

In this example we will cover the following functions:

```javascript
    var isColor = validator.isHexColor('#f1a0b3');
    var isEmail = validator.isEmail('test@t.com');
```

For more information, please refer to the documentation: https://github.com/chriso/validator.js#readme
