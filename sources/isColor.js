var validator = require('validator');

module.exports = function isColor() {
    var color = '#f1a0b3';

    var validated = validator.isHexColor(color);
    console.log(color, ' : Is it a color ? ', validated);

    return validated;
};