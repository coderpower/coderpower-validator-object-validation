var validator = require('validator');

module.exports = function inEmail() {
    var email = 'testé@t.com';

    var isEmailByRegex = /\S+@\S+\.\S+/.test(email);
    console.log('isEmailByRegex', isEmailByRegex);

    isEmailByValidator = validator.isEmail(email);
    console.log('isEmailByValidator', isEmailByValidator);

    isEmailByValidatorNoUTF8 = validator.isEmail(email, {allow_utf8_local_part: false});
    console.log('isEmailByValidatorNoUTF8', isEmailByValidatorNoUTF8);

    email = 'name <test@t.com>';

    isEmailByValidatorName = validator.isEmail(email, {allow_display_name: true});
    console.log('isEmailByValidatorName', isEmailByValidatorName);

    return isEmailByValidatorName;
};