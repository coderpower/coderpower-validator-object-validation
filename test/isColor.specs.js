var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var isColor = require('../sources/isColor');

describe('Object Validation : isColor', function() {

    it('Must be true', function() {
        var result = isColor();
        expect(result).to.be.true;
    });

});