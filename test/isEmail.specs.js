var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var isEmail = require('../sources/isEmail');

describe('Object Validation : isEmail', function() {

    it('Must be true', function() {
        var result = isEmail();
        expect(result).to.be.true;
    });

});